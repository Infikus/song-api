<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Song;
use AppBundle\Service\SocketClient;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class SongAPIController extends FOSRestController
{
    const FETCH_SONG = "FETCH_SONG_REQUEST";
    const SONG_DELETED = "SONG_DELETED";


    /**
     * @Rest\Get(path="/songs", name="app_get_songs")
     * @Rest\QueryParam(
     *     name="teamId",
     *     requirements="\d+",
     *     description="The team in which you want to search for songs"
     * )
     *
     * @View(statusCode=200,
     *     serializerGroups={"song", "song_team", "team", "song_songParams", "songParams"})
     * @param ParamFetcherInterface $paramFetcher
     * @return object
     */
    public function getSongsAction(ParamFetcherInterface $paramFetcher)
    {
        $teamId = $paramFetcher->get("teamId");

        if (!ctype_digit($teamId)) {
            return $this->view([
                "error" => "Invalid teamId query string"
            ], Response::HTTP_BAD_REQUEST);
        }

        $dm = $this->getDoctrine()->getManager();

        // Check if the user is in the team
        $teamRepo = $dm->getRepository("AppBundle:Team");
        $team = $teamRepo->getByUser($teamId, $this->getUser());
        if (!$team) {
            return $this->view([
                "error" => "The team " . $teamId . " is not valid"
            ], Response::HTTP_BAD_REQUEST);
        }

        $songRepo = $dm->getRepository("AppBundle:Song");

        $songs = $songRepo->getAllByTeam($team, $this->getUser());

        return $songs;
    }

    /**
     * @Rest\Get(path="/songs/{id}", name="app_show_song", requirements={"id"="\d+"})
     * @View(statusCode=200, serializerGroups={"song", "song_team", "team", "song_songParams", "songParams"})
     * @param int $id
     * @return object
     */
    public function showSongAction(int $id)
    {
        $dm = $this->getDoctrine()->getManager();
        $song = $dm->getRepository("AppBundle:Song")->get($id, $this->getUser());

        if (!$song || !$song->getTeam()) {
            return $this->view([
                "error" => "Invalid song"
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the user is in the right team
        $teamDb = $dm->getRepository("AppBundle:Team")
            ->getByUser($song->getTeam()->getId(), $this->getUser());
        if (!$teamDb) {
            return $this->view([
                "error" => "Invalid song list"
            ], Response::HTTP_BAD_REQUEST);
        }

        return $song;
    }


    /**
     * @Rest\Post(path="/songs", name="app_create_song")
     * @View(statusCode=201, serializerGroups={"song", "song_team", "team"})
     * @ParamConverter("song", converter="fos_rest.request_body", options={"validator"={"groups"={"Default", "creation"}}})
     *
     * @param Song $song
     * @param ConstraintViolationList $violations
     * @param SocketClient $socket
     * @return \FOS\RestBundle\View\View
     */
    public function createSongAction(Song $song, ConstraintViolationList $violations, SocketClient $socket)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        // Check if the team is valid
        $teamRepo = $em->getRepository("AppBundle:Team");
        $team = $teamRepo->getByUser($song->getTeam()->getId(), $this->getUser());
        if (!$team) {
            return $this->view([
                "error" => "The team " . $song->getTeam()->getId() . " is not valid"
            ], Response::HTTP_BAD_REQUEST);
        }
        $song->setTeam($team);

        $em->persist($song);
        $em->flush();

        $wsPayload = [
            "type" => self::FETCH_SONG,
            "payload" => [
                "songId" => $song->getId(),
            ]
        ];
        $socket->sendDataUpdated($team->getId(), $this->getUser()->getId(), $wsPayload);

        return $this->view($song, Response::HTTP_CREATED, [
            'Location' => $this->generateUrl("app_show_song", [
                "id" => $song->getId()
            ], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);
    }

    /**
     * @Rest\Put(path="/songs", name="app_update_song")
     * @View(statusCode=200, serializerGroups={"song", "song_team", "team"})
     * @ParamConverter("song", converter="fos_rest.request_body")
     *
     * @param Song $song
     * @param ConstraintViolationList $violations
     * @param SocketClient $socket
     * @return \FOS\RestBundle\View\View
     */
    public function updateSongAction(Song $song, ConstraintViolationList $violations, SocketClient $socket)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        // Check if the song exists
        $songDb = $em->getRepository("AppBundle:Song")->get($song->getId(), $this->getUser());
        if (!$songDb) {
            return $this->view([
                "error" => "The song " . $song->getId() . " does not exist"
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the team is valid
        $teamRepo = $em->getRepository("AppBundle:Team");
        $team = $teamRepo->getByUser($songDb->getTeam()->getId(), $this->getUser());
        if (!$team) {
            return $this->view([
                "error" => "You cannot use the team " . $songDb->getTeam()->getId()
            ], Response::HTTP_BAD_REQUEST);
        }
        $song->setTeam($songDb->getTeam());

        $em->merge($song);
        $em->flush();

        $wsPayload = [
            "type" => self::FETCH_SONG,
            "payload" => [
                "songId" => $song->getId(),
            ]
        ];
        $socket->sendDataUpdated($team->getId(), $this->getUser()->getId(), $wsPayload);

        return $this->view($song, Response::HTTP_CREATED, [
            'Location' => $this->generateUrl("app_show_song", [
                "id" => $song->getId()
            ], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);
    }

    /**
     * @Rest\Delete(path="/songs", name="app_delete_song")
     * @View(statusCode=200, serializerGroups={"song", "song_team", "team"})
     * @ParamConverter("song", converter="fos_rest.request_body")
     *
     * @param Song $song
     * @param ConstraintViolationList $violations
     * @param SocketClient $socket
     * @return Song|\FOS\RestBundle\View\View|null
     */
    public function deleteSongAction(Song $song, ConstraintViolationList $violations, SocketClient $socket) {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        // Check if the song exists
        $songDb = $em->getRepository("AppBundle:Song")->get($song->getId(), $this->getUser());
        if (!$songDb) {
            return $this->view([
                "error" => "The song " . $song->getId() . " does not exist"
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the team is valid
        $teamRepo = $em->getRepository("AppBundle:Team");
        $team = $teamRepo->getByUser($songDb->getTeam()->getId(), $this->getUser());
        if (!$team) {
            return $this->view([
                "error" => "Invalid song",
            ], Response::HTTP_BAD_REQUEST);
        }

        $em->remove($songDb);
        $em->flush();

        $wsPayload = [
            "type" => self::SONG_DELETED,
            "payload" => [
                "songId" => $song->getId(),
            ]
        ];
        $socket->sendDataUpdated($team->getId(), $this->getUser()->getId(), $wsPayload);

        return $songDb;
    }
}

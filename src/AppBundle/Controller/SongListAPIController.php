<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Song;
use AppBundle\Entity\SongList;
use AppBundle\Entity\SongListItem;
use AppBundle\Entity\SongParams;
use AppBundle\Representation\SongLists;
use AppBundle\Representation\Songs;
use AppBundle\Service\SocketClient;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class SongListAPIController extends FOSRestController
{
    const FETCH_SONG_LIST = "FETCH_SONG_LIST_REQUEST";
    const SONG_LIST_DELETED = "SONG_LIST_DELETED";

    /**
     * @Rest\Get(path="/song-lists", name="app_get_song_lists")
     * @Rest\QueryParam(
     *     name="teamId",
     *     requirements="\d+",
     *     description="The team in which you want to search for song lists"
     * )
     *
     * @View(statusCode=200,
     *     serializerGroups={"songList",
     *                      "songList_team",
     *                      "team",
     *                      "songList_songListItems",
     *                      "songListItem",
     *                      "songListItem_song",
     *                      "songListItem_songParams",
     *                      "songParams",
     *                      "song",
     *                      "song_songParams"})
     * @param ParamFetcherInterface $paramFetcher
     * @return object
     */
    public function getSongListsAction(ParamFetcherInterface $paramFetcher)
    {
        $teamId = $paramFetcher->get("teamId");

        if (!ctype_digit($teamId)) {
            return $this->view([
                "error" => "Invalid teamId query string"
            ], Response::HTTP_BAD_REQUEST);
        }

        $dm = $this->getDoctrine()->getManager();

        $songListRepo = $dm->getRepository("AppBundle:SongList");
        $teamRepo = $dm->getRepository("AppBundle:Team");

        $team = $teamRepo->getByUser($teamId, $this->getUser());
        if (!$team) {
            return $this->view([
                "error" => "The team " . $teamId . " is not valid"
            ], Response::HTTP_BAD_REQUEST);
        }


        $songLists = $songListRepo->getAllByTeam($team, $this->getUser());

        return $songLists;
    }

    /**
     * @Rest\Get(path="/song-lists/{id}", name="app_show_song_list", requirements={"id"="\d+"})
     * @View(statusCode=200,
     *     serializerGroups={"songList",
     *                      "songList_songListItems",
     *                      "songListItem",
     *                      "songListItem_song",
     *                      "song",
     *                      "songList_team",
     *                      "team",
     *                      "songListItem_songParams",
     *                      "songParams"})
     * @param int $id
     * @return object
     */
    public function showSongListAction(int $id)
    {
        $dm = $this->getDoctrine()->getManager();
        $songList = $dm->getRepository("AppBundle:SongList")->getByUser($id, $this->getUser());

        if (!$songList || !$songList->getTeam()) {
            return $this->view([
                "error" => "Invalid song list"
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the user is in the right team
        $teamDb = $dm->getRepository("AppBundle:Team")
            ->getByUser($songList->getTeam()->getId(), $this->getUser());
        if (!$teamDb) {
            return $this->view([
                "error" => "Invalid song list"
            ], Response::HTTP_BAD_REQUEST);
        }

        return $songList;
    }

    /**
     * @Rest\Post(path="/song-lists", name="app_create_song_list")
     * @View(statusCode=201,
     *     serializerGroups={"songList",
     *                      "songList_songListItems",
     *                      "songListItem",
     *                      "songListItem_song",
     *                      "song",
     *                      "songList_team",
     *                      "team",
     *                      "songListItem_songParams",
     *                      "songParams"})
     * @ParamConverter("songList", converter="fos_rest.request_body", options={"validator"={"groups"={"Default", "creation"}}})
     *
     * @param SongList $songList
     * @param ConstraintViolationList $violations
     * @param SocketClient $socket
     * @return \FOS\RestBundle\View\View
     */
    public function createSongListAction(SongList $songList, ConstraintViolationList $violations, SocketClient $socket)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        // Check if the user is in the right team
        $teamDb = $em->getRepository("AppBundle:Team")
            ->getByUser($songList->getTeam()->getId(), $this->getUser());
        if (!$teamDb) {
            return $this->view([
                "error" => "Invalid team"
            ], Response::HTTP_BAD_REQUEST);
        }
        $songList->setTeam($teamDb);

        $songRepo = $em->getRepository('AppBundle:Song');

        // Get songs and check if they exist.
        if ($songList->getSongListItems()) {
            foreach ($songList->getSongListItems() as $key => $songListItem) {
                $song = $songListItem->getSong();
                if ($song->getId() == null) {
                    return $this->view([
                        "error" => "All songs needs to have an id."
                    ], Response::HTTP_BAD_REQUEST);
                }
                $dbSong = $songRepo->get($song->getId());

                // The song must be in the same team as the songList
                if (!$dbSong->getTeam() || $dbSong->getTeam()->getId() !== $teamDb->getId()) {
                    return $this->view([
                        "error" => "Invalid song " . $song->getId()
                    ], Response::HTTP_BAD_REQUEST);
                }

                if (!$dbSong) {
                    return $this->view([
                        "error" => "The song with the id " . $song->getId() . " doesn't exists"
                    ], Response::HTTP_BAD_REQUEST);
                }

                $songListItem
                    ->setSong($dbSong);
            }
        }

        $em->persist($songList);
        $em->flush();

        $wsPayload = [
            "type" => self::FETCH_SONG_LIST,
            "payload" => [
                "songListId" => $songList->getId(),
            ],
        ];
        $socket->sendDataUpdated($teamDb->getId(), $this->getUser()->getId(), $wsPayload);

        return $this->view($songList, Response::HTTP_CREATED, [
            'Location' => $this->generateUrl("app_show_song_list", [
                "id" => $songList->getId()
            ], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);
    }

    /**
     * @Rest\Put(path="/song-lists", name="app_update_song_list")
     * @View(statusCode=200,
     *     serializerGroups={"songList",
     *                      "songList_songListItems",
     *                      "songListItem",
     *                      "songListItem_song",
     *                      "song",
     *                      "songList_team",
     *                      "team",
     *                      "songListItem_songParams",
     *                      "songParams"})
     * @ParamConverter("songList", converter="fos_rest.request_body")
     *
     * @param SongList $songList
     * @param ConstraintViolationList $violations
     * @param SocketClient $socket
     * @return \FOS\RestBundle\View\View
     */
    public function updateSongAction(SongList $songList, ConstraintViolationList $violations, SocketClient $socket)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        // Check if the songList exists
        $songListDb = $em->getRepository('AppBundle:SongList')->get($songList->getId());
        if (!$songListDb) {
            return $this->view([
                "error" => "The song list with the id " . $songList->getId() . " doesn't exists"
            ], Response::HTTP_BAD_REQUEST);
        }


        // Check if the user is in the right team
        $teamDb = $em->getRepository("AppBundle:Team")
            ->getByUser($songListDb->getTeam()->getId(), $this->getUser());
        if (!$teamDb) {
            return $this->view([
                "error" => "Invalid team"
            ], Response::HTTP_BAD_REQUEST);
        }

        // We will need to get all the members of the team
        $teamMembers = $em->getRepository("AppBundle:TeamMember")
            ->getAllByTeam($teamDb);

        $songRepo = $em->getRepository('AppBundle:Song');

        // Get songs and check if they exist.
        $allIds = [];
        if ($songList->getSongListItems()) {
            // Update and add Items
            foreach ($songList->getSongListItems() as $key => $songListItem) {
                // There must be a song with a valid id
                $song = $songListItem->getSong();
                if (!$song || $song->getId() == null) {
                    return $this->view([
                        "error" => "All songs needs to have an id."
                    ], Response::HTTP_BAD_REQUEST);
                }
                $dbSong = $songRepo->find($song->getId());

                if (!$dbSong) {
                    return $this->view([
                        "error" => "The song with the id " . $song->getId() . " doesn't exists"
                    ], Response::HTTP_BAD_REQUEST);
                }

                // The song must be in the same team as the songList
                if (!$dbSong->getTeam() || $dbSong->getTeam()->getId() !== $teamDb->getId()) {
                    return $this->view([
                        "error" => "Invalid song " . $song->getId()
                    ], Response::HTTP_BAD_REQUEST);
                }

                // If there is an id, check if it is correct
                $itemId = $songListItem->getId();
                if ($itemId !== null) {
                    $isInDb = false;
                    foreach($songListDb->getSongListItems() as $itemDb) {
                        if ($itemDb->getId() === $itemId) {
                            // Update the item
                            $itemDb->setPosition($songListItem->getPosition());
                            $allIds[] = $itemId;
                            $isInDb = true;
                            break;
                        }
                    }
                    if (!$isInDb) {
                        return $this->view([
                            "error" => "Invalid songListItem " . $itemId
                        ], Response::HTTP_BAD_REQUEST);
                    }

                } else {
                    $newSongListItem = new SongListItem();

                    $newSongListItem
                        ->setSongList($songListDb)
                        ->setSong($dbSong)
                        ->setPosition($songListItem->getPosition());

                    // Create a SongParams for each member of the team using the default SongParams of the song
                    // First, get the SongParams for the song
                    $originalSongParams = $em->getRepository("AppBundle:SongParams")
                        ->getByUserAndSong($this->getUser(), $dbSong);

                    if ($originalSongParams) {
                        $newSongListItem->setTranspose($originalSongParams->getTranspose());
                    }

                    foreach ($teamMembers as $teamMember) {
                        $newParams = new SongParams();

                        // Get the songParams of the selected user to get the capo
                        $selectedUserSongParams = $em->getRepository("AppBundle:SongParams")
                            ->getByUserAndSong($teamMember->getUser(), $dbSong);
                        if ($selectedUserSongParams) {
                            $newParams->setCapo($selectedUserSongParams->getCapo());
                        }

                        $newParams->setUser($teamMember->getUser());
                        $newParams->setSongListItem($newSongListItem);

                        // We want that the songListItem that will be return only contains the song params of the logged in user
                        if ($teamMember->getUser()->getId() === $this->getUser()->getId()) {
                            $newSongListItem->addSongParam($newParams);
                        } else {
                            $em->persist($newParams);
                        }
                    }

                    $songListDb->addSongListItem($newSongListItem);
                }
            }

            // Remove unused items
            foreach($songListDb->getSongListItems() as $songListItem) {
                $itemId = $songListItem->getId();
                if ($itemId !== null && !in_array($itemId, $allIds)) {
                    // Remove the item in the collection and remove it from the database
                    $songListDb->removeSongListItem($songListItem);
                    $em->remove($songListItem);
                }
            }
        }

        $songListDb->setName($songList->getName());
        $songListDb->setDate($songList->getDate());

        $em->persist($songListDb);
        $em->flush();

        $wsPayload = [
            "type" => self::FETCH_SONG_LIST,
            "payload" => [
                "songListId" => $songList->getId(),
            ],
        ];
        $socket->sendDataUpdated($teamDb->getId(), $this->getUser()->getId(), $wsPayload);

        return $songListDb;
    }


    /**
     * @Rest\Delete(path="/song-lists", name="app_delete_song_list")
     * @View(statusCode=200,
     *     serializerGroups={"songList",
     *                      "songList_songListItems",
     *                      "songListItem",
     *                      "songListItem_song",
     *                      "song",
     *                      "songList_team",
     *                      "team",
     *                      "songListItem_songParams",
     *                      "songParams"})
     * @ParamConverter("songList", converter="fos_rest.request_body")
     *
     * @param SongList $songList
     * @param ConstraintViolationList $violations
     * @param SocketClient $socket
     * @return \FOS\RestBundle\View\View
     */
    public function deleteSongAction(SongList $songList, ConstraintViolationList $violations, SocketClient $socket)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $dm = $this->getDoctrine()->getManager();

        $songListDb = $dm->getRepository("AppBundle:SongList")->get($songList->getId());
        if (!$songListDb) {
            return $this->view([
                "error" => "Invalid song list"
            ], Response::HTTP_NOT_FOUND);
        }

        // Check if the user is in the right team
        $teamDb = $dm->getRepository("AppBundle:Team")
            ->getByUser($songListDb->getTeam()->getId(), $this->getUser());
        if (!$teamDb) {
            return $this->view([
                "error" => "Invalid song list"
            ], Response::HTTP_BAD_REQUEST);
        }

        $dm->remove($songListDb);
        $dm->flush();

        $wsPayload = [
            "type" => self::SONG_LIST_DELETED,
            "payload" => [
                "songListId" => $songList->getId(),
            ],
        ];
        $socket->sendDataUpdated($teamDb->getId(), $this->getUser()->getId(), $wsPayload);

        return $songListDb;
    }
}

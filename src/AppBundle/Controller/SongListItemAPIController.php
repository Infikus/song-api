<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SongListItem;
use AppBundle\Service\SocketClient;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;

class SongListItemAPIController extends FOSRestController
{
    const FETCH_SONG_LIST = "FETCH_SONG_LIST_REQUEST";

    /**
     * @Rest\Put(path="/song-list-items", name="app_update_song_list_item")
     * @View(statusCode=200,
     *     serializerGroups={"songListItem",
     *                      "songListItem_song",
     *                      "song"})
     * @ParamConverter("item", converter="fos_rest.request_body")
     *
     * @param SongListItem $item
     * @param ConstraintViolationList $violations
     * @param SocketClient $socket
     * @return \FOS\RestBundle\View\View
     */
    public function updateSongListItemAction(SongListItem $item, ConstraintViolationList $violations, SocketClient $socket)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        $itemDb = $em->getRepository("AppBundle:SongListItem")->get($item->getId(), $this->getUser());
        if (!$itemDb) {
            return $this->view([
                "error" => "The songListItem " . $item->getId() . " is not valid"
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the user is in the right team
        $teamDb = $em->getRepository("AppBundle:Team")
            ->getByUser($itemDb->getSong()->getTeam()->getId(), $this->getUser());
        if (!$teamDb) {
            return $this->view([
                "error" => "Invalid songListItem (not in the team)"
            ], Response::HTTP_BAD_REQUEST);
        }

        // Everything is ok
        // Update only transpose
        if ($item->getTranspose() !== null) {
            $itemDb->setTranspose($item->getTranspose());
        }

        $em->persist($itemDb);
        $em->flush();

        $wsPayload = [
            "type" => self::FETCH_SONG_LIST,
            "payload" => [
                "songListId" => $itemDb->getSongList()->getId(),
            ],
        ];
        $socket->sendDataUpdated($teamDb->getId(), $this->getUser()->getId(), $wsPayload);

        return $itemDb;
    }
}

<?php

namespace AppBundle\Controller;


use AppBundle\Entity\SongParams;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;

class SongParamsAPIController extends FOSRestController
{

    /**
     * WARNING : If a songParams with the same (songId, userId) already exists, it will be updated (Not really Restful...)
     *
     * @Rest\Post(path="/song-params", name="app_create_songParams")
     * @Rest\View(statusCode="201",
     *     serializerGroups={"songParams",
     *                       "songParams_song",
     *                       "song",
     *                       "songParams_songListItem",
     *                       "songListItem",
     *                       "songParams_user",
     *                       "user"})
     *
     * @ParamConverter("songParams", converter="fos_rest.request_body")
     *
     * @param SongParams $songParams
     * @param ConstraintViolationList $violations
     * @return \FOS\RestBundle\View\View
     */
    public function createSongParamsAction(SongParams $songParams, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $hasSong = $songParams->getSong() !== null && $songParams->getSong()->getId() !== null;
        $hasSongListItem = $songParams->getSongListItem() !== null && $songParams->getSongListItem()->getId() !== null;

        if (!$hasSong && !$hasSongListItem) {
            return $this->view([
                "error" => "There must be a song or a songListItem"
            ], Response::HTTP_BAD_REQUEST);
        } else if ($hasSong && $hasSongListItem) {
            return $this->view([
                "error" => "There cannot be a song and a songListItem"
            ], Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $songParams->setUser($user);

        $songDb = null;
        $songListItemDb = null;
        $teamId = null;
        if ($hasSong) {
            // Check the song
            $songRepo = $em->getRepository("AppBundle:Song");
            $songDb = $songRepo->get($songParams->getSong()->getId(), $user);
            if (!$songDb) {
                return $this->view([
                    "error" => "The song " . $songParams->getSong()->getId() . " is not valid"
                ], Response::HTTP_BAD_REQUEST);
            }
            $teamId = $songDb->getTeam()->getId();
        } else {
            // Check the songListItem
            $songListItemRepo = $em->getRepository("AppBundle:SongListItem");
            $songListItemDb = $songListItemRepo->get($songParams->getSongListItem()->getId(), $user);
            if (!$songListItemDb) {
                return $this->view([
                    "error" => "The songListItem " . $songParams->getSongListItem()->getId() . " is not valid"
                ], Response::HTTP_BAD_REQUEST);
            }
            $teamId = $songListItemDb->getSong()->getTeam()->getId();
        }

        // Check the team
        $teamRepo = $em->getRepository("AppBundle:Team");
        $team = $teamRepo->getByUser($teamId, $user);
        if (!$team) {
            return $this->view([
                "error" => "The team " . $teamId . " is not valid"
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if there was already a songParams with same song and user
        $songParamsRepo = $em->getRepository("AppBundle:SongParams");
        if ($hasSong) {
            $songParamsDb = $songParamsRepo->getByUserAndSong($user, $songDb);
        } else {
            $songParamsDb = $songParamsRepo->getByUserAndSongListItem($user, $songListItemDb);
        }
        if ($songParamsDb) {
            $songParamsDb->setCapo($songParams->getCapo());
            $songParamsDb->setTranspose($songParams->getTranspose());

            $em->flush();

            return $this->view($songParamsDb);
        }

        // Everything is OK, let's persist the new entity
        $songParams->setSong($songDb);
        $songParams->setSongListItem($songListItemDb);

        $em->persist($songParams);
        $em->flush();

        return $this->view($songParams);
    }
}
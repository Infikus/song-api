<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Song;
use AppBundle\Entity\Team;
use AppBundle\Entity\TeamMember;
use AppBundle\Representation\Songs;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class TeamAPIController extends FOSRestController
{
    /**
     * This action create a new team and add the logged user in this team
     * This returns a TeamMember object
     *
     * @Rest\Post(path="/teams", name="app_create_team")
     * @View(statusCode=201,
     *     serializerGroups={"team",
     *                      "team_teamMembers",
     *                      "teamMember",
     *                      "teamMember_user",
     *                      "user"})
     * @ParamConverter("team", converter="fos_rest.request_body")
     *
     * @param Team $team
     * @param ConstraintViolationList $violations
     * @return \FOS\RestBundle\View\View
     */
    public function createTeamAction(Team $team, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $team->setCreatedAt(new \DateTime("now"));
        $team->setPrivate(false); // You cannot create a private team (only created at registration)

        $teamMember = new TeamMember();
        $teamMember->setUser($user);
        $teamMember->setTeam($team);
        $teamMember->setRole(TeamMember::TEAM_MASTER);

        $team->addTeamMember($teamMember);

        $em->persist($teamMember);
        $em->flush();

        return $this->view($team, Response::HTTP_CREATED);
    }

    /**
     * This action update the team passed in the request body (only update name and description)
     *
     * @Rest\Put(path="/teams", name="app_update_team")
     * @View(statusCode=200,
     *     serializerGroups={"team",
     *                      "team_teamMembers",
     *                      "teamMember",
     *                      "teamMember_user",
     *                      "user"})
     * @ParamConverter("team", converter="fos_rest.request_body")
     *
     * @param Team $team
     * @param ConstraintViolationList $violations
     * @return object
     */
    public function updateTeamAction(Team $team, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        // Check if the team exists
        $em = $this->getDoctrine()->getManager();
        $teamRepo = $em->getRepository('AppBundle:Team');

        $teamDb = $teamRepo->get($team->getId());
        if (!$teamDb) {
            return $this->view([
                "error" => "The team with the id " . $team->getId() . " doesn't exists."
            ], Response::HTTP_BAD_REQUEST);
        }

        // And if the user has the right role in this team
        $member = $em->getRepository('AppBundle:TeamMember')->getByUserAndTeam($this->getUser(), $teamDb);

        if (!$member || $member->getRole() !== TeamMember::TEAM_MASTER) {
            return $this->view([
                "error" => "You are not allowed to perform this operation"
            ], Response::HTTP_FORBIDDEN);
        }

        $teamDb->setName($team->getName());
        $teamDb->setDescription($team->getDescription());

        $em->merge($teamDb);
        $em->flush();

        return $teamDb;
    }
    /**
     * This action delete the team passed in the request body
     *
     * @Rest\Delete(path="/teams", name="app_delete_team")
     * @View(statusCode=200,
     *     serializerGroups={"team",
     *                      "team_teamMembers",
     *                      "teamMember",
     *                      "teamMember_user",
     *                      "user"})
     * @ParamConverter("team", converter="fos_rest.request_body")
     *
     * @param Team $team
     * @param ConstraintViolationList $violations
     * @return object
     */
    public function deleteTeamAction(Team $team, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        // Check if the team exists
        $em = $this->getDoctrine()->getManager();
        $teamRepo = $em->getRepository('AppBundle:Team');

        $teamDb = $teamRepo->get($team->getId());
        if (!$teamDb) {
            return $this->view([
                "error" => "The team with the id " . $team->getId() . " doesn't exists."
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($teamDb->getPrivate()) {
            return $this->view([
                "error" => "You cannot delete a private team."
            ], Response::HTTP_BAD_REQUEST);
        }

        // And if the user has the right role in this team
        $member = $em->getRepository('AppBundle:TeamMember')->getByUserAndTeam($this->getUser(), $teamDb);

        if (!$member || $member->getRole() !== TeamMember::TEAM_MASTER) {
            return $this->view([
                "error" => "You are not allowed to perform this operation"
            ], Response::HTTP_FORBIDDEN);
        }

        $em->remove($teamDb);
        $em->flush();

        return $teamDb;
    }

    /**
     * Return all teams
     *
     * @Rest\Get(path="/teams", name="app_get_teams")
     * @View(statusCode=200,
     *     serializerGroups={"team"})
     *
     * @return \FOS\RestBundle\View\View
     */
    public function getTeamsActions()
    {
        $teamRepo = $this->getDoctrine()->getManager()->getRepository("AppBundle:Team");

        $teams = $teamRepo->getAllByUser($this->getUser());
        return $teams;
    }

    /**
     * @Rest\Get(path="/teams/{id}", name="app_show_team", requirements={"id"="\d+"})
     * @View(statusCode=200,
     *     serializerGroups={"team",
     *                      "team_teamMembers",
     *                      "teamMember",
     *                      "teamMember_user",
     *                      "user"})
     * @param int $id
     * @return object
     */
    public function showTeamAction(int $id)
    {
        $team = $this->getDoctrine()->getManager()->getRepository("AppBundle:Team")->getByUser($id, $this->getUser());

        if (!$team) {
            return $this->view([
                "error" => "The team with the id " . $id . " is invalid."
            ], Response::HTTP_BAD_REQUEST);
        }

        return $team;
    }

    /**
     * This action adds the user in the team with the role WAITING_ANSWER
     *
     * @Rest\Post(path="/team-members", name="app_join_team")
     * @View(statusCode=200,
     *     serializerGroups={"teamMember",
     *                      "teamMember_team",
     *                      "team",
     *                      "teamMember_user",
     *                      "user"})
     * @ParamConverter("teamMember", converter="fos_rest.request_body")
     *
     * @param TeamMember $teamMember
     * @param ConstraintViolationList $violations
     * @return object
     */
    public function addTeamMemberAction(TeamMember $teamMember, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $err = null;
        if ($teamMember->getUser()->getEmail() == null) {
            $err = "user.email should not be null";
        } else if ($teamMember->getTeam()->getId() == null) {
            $err = "team.id should not be null";
        }

        if ($err !== null) {
            return $this->view([
                "error" => $err
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the team exists
        $em = $this->getDoctrine()->getManager();
        $teamRepo = $em->getRepository('AppBundle:Team');

        $teamDb = $teamRepo->get($teamMember->getTeam()->getId());
        if (!$teamDb) {
            return $this->view([
                "error" => "The team with the id " . $teamMember->getId() . " doesn't exists."
            ], Response::HTTP_NOT_FOUND);
        }

        // You cannot add a member in a private team
        if ($teamDb->getPrivate()) {
            return $this->view([
                "error" => "You cannot add a member in a private team."
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the logged user is a master in the team
        $teamMemberRepo = $em->getRepository("AppBundle:TeamMember");
        $loggedTeamMember = $teamMemberRepo->getByUserAndTeam($this->getUser(), $teamDb);

        if (!$loggedTeamMember || $loggedTeamMember->getRole() !== TeamMember::TEAM_MASTER) {
            return $this->view([
                "error" => "You are not allowed to perform this operation."
            ], Response::HTTP_FORBIDDEN);
        }

        // Check if the user exists
        $userRepo = $em->getRepository("AppBundle:User");
        $userDb = $userRepo->getByEmail($teamMember->getUser()->getEmail());

        if (!$userDb) {
            // TODO: Envoie d'un email d'invitation
            return $this->view([
                "error" => "The user does not exist"
            ], Response::HTTP_NOT_FOUND);
        }

        // Check if the userDb is not already in the team
        $teamMemberDb = $teamMemberRepo->getByUserAndTeam($userDb, $teamDb);
        if ($teamMemberDb !== null) {
            return $this->view([
                "error" => "Already in the team."
            ], Response::HTTP_CONFLICT);
        }

        // Everything is ok
        $teamMember->setTeam($teamDb);
        $teamMember->setUser($userDb);

        $em->persist($teamMember);
        $em->flush();

        return $teamMember;
    }

    /**
     * This action update a teamMember (ie role)
     *
     * @Rest\Put(path="/team-members", name="app_delete_team_update")
     * @View(statusCode=200,
     *     serializerGroups={"teamMember",
     *                      "teamMember_user",
     *                      "user",
     *                      "teamMember_team",
     *                      "team"})
     * @ParamConverter("teamMember", converter="fos_rest.request_body")
     *
     * @param TeamMember $teamMember
     * @param ConstraintViolationList $violations
     * @return \FOS\RestBundle\View\View
     */
    public function updateTeamMemberAction(TeamMember $teamMember, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }


        $em = $this->getDoctrine()->getManager();

        $teamMemberRepo = $em->getRepository("AppBundle:TeamMember");

        // Check if the teamMember exists in db
        $teamMemberDb = $teamMemberRepo->get($teamMember->getId());
        if (!$teamMemberDb) {
            return $this->view([
                "error" => "The team member ".$teamMember->getId()." doesn't exist.",
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the logged user is a member of the team
        $loggedTeamMember = $teamMemberRepo->getByUserAndTeam($this->getUser(), $teamMemberDb->getTeam());
        if (!$loggedTeamMember) {
            return $this->view([
                "error" => "You are not a member of the team.",
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if the logged user is allowed to update the team member
        if ($loggedTeamMember->getRole() != TeamMember::TEAM_MASTER) {
            return $this->view([
                "error" => "You are not allowed to perform this operation"
            ], Response::HTTP_FORBIDDEN);
        }

        if ($teamMember->getRole() !== null) {
            $teamMemberDb->setRole($teamMember->getRole());
        }

        // If the user want to set his role to team_member, there must be another team_master
        if ($teamMemberDb->getRole() !== TeamMember::TEAM_MASTER and
            $teamMemberDb->getId() === $loggedTeamMember->getId()) {

            $teamMembers = $teamMemberRepo->getAllByTeam($teamMemberDb->getTeam());
            $hasAnotherMaster = false;
            foreach ($teamMembers as $teamMember) {
                if ($teamMember->getRole() === TeamMember::TEAM_MASTER and
                    $teamMember->getId() !== $loggedTeamMember->getId()) {
                    $hasAnotherMaster = true;
                    break;
                }
            }

            if (!$hasAnotherMaster) {
                return $this->view([
                    "error" => "There must be another master in the team"
                ], Response::HTTP_BAD_REQUEST);
            }
        }

        // Everything is ok
        $em->persist($teamMemberDb);
        $em->flush();

        return $teamMemberDb;
    }
    /**
     * This action remove an user in a team
     *
     * @Rest\Delete(path="/team-members", name="app_delete_team_member")
     * @View(statusCode=200,
     *     serializerGroups={"teamMember",
     *                      "teamMember_user",
     *                      "user",
     *                      "teamMember_team",
     *                      "team"})
     * @ParamConverter("teamMember", converter="fos_rest.request_body")
     *
     * @param TeamMember $teamMember
     * @param ConstraintViolationList $violations
     * @return \FOS\RestBundle\View\View
     */
    public function deleteTeamMemberAction(TeamMember $teamMember, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            return $this->view($violations, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $teamMemberRepo = $em->getRepository("AppBundle:TeamMember");

        // Check if the teamMember exists
        $teamMemberDb = $teamMemberRepo->get($teamMember->getId());
        if (!$teamMemberDb) {
            return $this->view([
                "error" => "The team member ".$teamMember->getId()." doesn't exist.",
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getUser();

        // Check if the logged user is in the team and can delete the teamMember
        $loggedTeamMember = $teamMemberRepo->getByUserAndTeam($user, $teamMemberDb->getTeam());
        if (!$loggedTeamMember) {
            return $this->view([
                "error" => "Not a member of the team"
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($teamMemberDb->getUser()->getId() !== $user->getId()) {
            // If the user want to delete another user
            if ($loggedTeamMember->getRole() !== TeamMember::TEAM_MASTER) {
                return $this->view([
                    "error" => "You must be a master"
                ], Response::HTTP_FORBIDDEN);
            }

        } else {
            // The user want to delete himself
            if ($teamMemberDb->getRole() === TeamMember::TEAM_MASTER) {
                // If he is a master, there must be another one in the team
                $teamMembers = $teamMemberRepo->getAllByTeam($teamMemberDb->getTeam());
                $hasAnotherMaster = false;
                foreach ($teamMembers as $teamMember) {
                    if ($teamMember->getRole() === TeamMember::TEAM_MASTER and
                        $teamMember->getUser()->getId() !== $user->getId()) {
                        $hasAnotherMaster = true;
                        break;
                    }
                }

                if (!$hasAnotherMaster) {
                    return $this->view([
                        "error" => "There must be another master in the team"
                    ], Response::HTTP_BAD_REQUEST);
                }
            }
        }

        $em->remove($teamMemberDb);
        $em->flush();

        return $teamMemberDb;
    }
}

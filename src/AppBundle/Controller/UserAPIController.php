<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Team;
use AppBundle\Entity\TeamMember;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserAPIController extends FOSRestController
{

    /**
     * @Rest\Get(path="/me", name="app_me")
     * @Rest\View(statusCode=200,
     *     serializerGroups={"user",
     *                      "user_teamMembers",
     *                      "user_lastTeamMember",
     *                      "teamMember",
     *                      "teamMember_team",
     *                      "team"})
     *
     * @return object
     */
    public function meAction()
    {
        return $this->getUser();
    }

    /**
     * @Rest\Post(path="/register", name="app_register")
     * @Rest\View(statusCode=201,
     *     serializerGroups={"user",
     *                      "user_teamMembers",
     *                      "user_lastTeamMember",
     *                      "teamMember",
     *                      "teamMember_team",
     *                      "team"})
     *
     * @param Request $request
     * @return object
     */
    public function registerAction(Request $request)
    {
        /** @var \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm(array('csrf_protection' => false));
        $form->setData($user);
        $this->processRegistrationForm($request, $form);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(
                FOSUserEvents::REGISTRATION_SUCCESS, $event
            );

            $user->setEnabled(true);
            $user->addRole('ROLE_USER');

            // Create a new private team
            $team = new Team();
            $team->setPrivate(true);
            $team->setName("___PRIVATE___" . $user->getId());

            $teamMember = new TeamMember();
            $teamMember->setUser($user)
                ->setRole(TeamMember::TEAM_MASTER)
                ->setTeam($team);

            $user->setLastTeamMember($teamMember);
            $user->addTeamMember($teamMember);
            $userManager->updateUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($teamMember);
            $em->flush();

            return $user;
        }

        $errors = $this->getErrorsFromForm($form);
        $errorCode = Response::HTTP_BAD_REQUEST;
        $errorsData = [];
        if (array_key_exists("password", $errors)) {
            $errorsData[] = "Mot de passe invalide.";
        }
        if (array_key_exists("email", $errors)) {
            switch ($errors["email"][0]) {
                case "fos_user.email.invalid":
                    $errorsData[] = "Veuillez entrer une adresse mail valide.";
                    break;
                case "fos_user.email.already_used":
                    $errorsData[] = "Adresse mail déjà utilisée";
                    $errorCode = Response::HTTP_CONFLICT;
                    break;
            }
        }

        if (!count($errorsData))
            $errorsData[] = "Une erreur est survenue.";

        return $this->view([
            "error" => implode("\n", $errorsData),
        ], $errorCode);
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }

    /**
     * @param  Request $request
     * @param  FormInterface $form
     */
    private function processRegistrationForm(Request $request, FormInterface $form)
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            throw new BadRequestHttpException();
        } else if (!$data["password"] || !$data["email"]) {
            throw new BadRequestHttpException();
        }

        $user = [
            "username" => $data["email"],
            "email" => $data["email"],
            "plainPassword" => [
                "first" => $data["password"],
                "second" => $data["password"],
            ],
        ];

        $form->submit($user);
    }
}

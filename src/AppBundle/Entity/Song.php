<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Song
 *
 * @ORM\Table(name="song")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SongRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Song
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"song"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"song"})
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"song"})
     */
    private $author;

    /**
     * Valeur : 0 = Do, 1 = Ré, etc... avec 5 = Fa et -6 = F#
     *
     * @var int
     *
     * @ORM\Column(name="`key`", type="integer", nullable=true)
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"song"})
     */
    private $key;

    /**
     * @var int
     *
     * @ORM\Column(name="bpm", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(value=0)
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"song"})
     */
    private $bpm;

    /**
     * @var string
     *
     * @ORM\Column(name="time_sig", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"song"})
     */
    private $timeSig;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"song"})
     */
    private $content;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="SongListItem", mappedBy="song", cascade={"persist", "merge", "remove"})
     * @Serializer\Groups({"song_items"})
     * @Serializer\Expose()
     */
    private $songListItems;

    /**
     * @var Team
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="songs")
     * @Assert\NotNull(groups={"creation"})
     * @Serializer\Groups({"song_team"})
     * @Serializer\Expose()
     */
    private $team;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SongParams", mappedBy="song", cascade={"remove", "persist"})
     * @Serializer\Groups({"song_songParams"})
     * @Serializer\Expose()
     */
    private $songParams;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Song
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Song
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Song
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set key
     *
     * @param integer $key
     *
     * @return Song
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return integer
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set bpm
     *
     * @param integer $bpm
     *
     * @return Song
     */
    public function setBpm($bpm)
    {
        $this->bpm = $bpm;

        return $this;
    }

    /**
     * Get bpm
     *
     * @return integer
     */
    public function getBpm()
    {
        return $this->bpm;
    }

    /**
     * Set timeSig
     *
     * @param string $timeSig
     *
     * @return Song
     */
    public function setTimeSig($timeSig)
    {
        $this->timeSig = $timeSig;

        return $this;
    }

    /**
     * Get timeSig
     *
     * @return string
     */
    public function getTimeSig()
    {
        return $this->timeSig;
    }

    /**
     * Add songListItem
     *
     * @param \AppBundle\Entity\SongListItem $songListItem
     *
     * @return Song
     */
    public function addSongListItem(\AppBundle\Entity\SongListItem $songListItem)
    {
        $this->songListItems[] = $songListItem;

        return $this;
    }

    /**
     * Remove songListItem
     *
     * @param \AppBundle\Entity\SongListItem $songListItem
     */
    public function removeSongListItem(\AppBundle\Entity\SongListItem $songListItem)
    {
        $this->songListItems->removeElement($songListItem);
    }

    /**
     * Get songListItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSongListItems()
    {
        return $this->songListItems;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->songListItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     *
     * @return Song
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Add songParam
     *
     * @param \AppBundle\Entity\SongParams $songParam
     *
     * @return Song
     */
    public function addSongParam(\AppBundle\Entity\SongParams $songParam)
    {
        $this->songParams[] = $songParam;

        return $this;
    }

    /**
     * Remove songParam
     *
     * @param \AppBundle\Entity\SongParams $songParam
     */
    public function removeSongParam(\AppBundle\Entity\SongParams $songParam)
    {
        $this->songParams->removeElement($songParam);
    }

    /**
     * Get songParams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSongParams()
    {
        return $this->songParams;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SongList
 *
 * @ORM\Table(name="song_list")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SongListRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SongList
{
    /**
     * @var int
     * @Serializer\Groups({"songList"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Serializer\Groups({"songList"})
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     * @Assert\NotBlank()
     * @Serializer\Groups({"songList"})
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var \DateTime
     * @Serializer\Expose()
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @Serializer\Groups({"songList"})
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="SongListItem", mappedBy="songList", cascade={"persist", "merge", "remove"})
     * @Serializer\Groups({"songList_songListItems"})
     */
    private $songListItems;

    /**
     * @var Team
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="songLists")
     * @Assert\NotNull(groups={"creation"})
     * @Serializer\Groups({"songList_team"})
     * @Serializer\Type("AppBundle\Entity\Team")
     */
    private $team;

    public function __construct() {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
    * @ORM\PreUpdate
    * @ORM\PrePersist
    */
    public function updateDates()
    {
        $this->updatedAt = new \DateTime("now");
        if ($this->createdAt === null) {
            $this->createdAt = new \DateTime("now");
        }
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SongList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return SongList
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SongList
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return SongList
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add songListItem
     *
     * @param \AppBundle\Entity\SongListItem $songListItem
     *
     * @return SongList
     */
    public function addSongListItem(\AppBundle\Entity\SongListItem $songListItem)
    {
        $this->songListItems[] = $songListItem;

        return $this;
    }

    /**
     * Remove songListItem
     *
     * @param \AppBundle\Entity\SongListItem $songListItem
     */
    public function removeSongListItem(\AppBundle\Entity\SongListItem $songListItem)
    {
        $this->songListItems->removeElement($songListItem);
    }

    /**
     * Get songListItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSongListItems()
    {
        return $this->songListItems;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     *
     * @return SongList
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SongListItem
 *
 * @ORM\Table(name="song_list_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SongListItemRepository")
 */
class SongListItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\NotNull()
     * @Serializer\Groups({"songListItem"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     * @Serializer\Groups({"songListItem"})
     */
    private $position;

    /**
     * @var int
     *
     * @ORM\Column(name="transpose", type="integer")
     * @Serializer\Groups({"songListItem"})
     */
    private $transpose;

    /**
     * @var SongList
     *
     * @ORM\ManyToOne(targetEntity="SongList", inversedBy="songListItems")
     * @Serializer\Groups({"songListItem_songList"})
     */
    private $songList;

    /**
     * @var Song
     *
     * @ORM\ManyToOne(targetEntity="Song", inversedBy="songListItems")
     * @Serializer\Groups({"songListItem_song"})
     */
    private $song;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SongParams", mappedBy="songListItem", cascade={"remove", "persist"})
     * @Serializer\Groups({"songListItem_songParams"})
     */
    private $songParams;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return SongListItem
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set songList
     *
     * @param \AppBundle\Entity\SongList $songList
     *
     * @return SongListItem
     */
    public function setSongList(\AppBundle\Entity\SongList $songList = null)
    {
        $this->songList = $songList;

        return $this;
    }

    /**
     * Get songList
     *
     * @return \AppBundle\Entity\SongList
     */
    public function getSongList()
    {
        return $this->songList;
    }

    /**
     * Set song
     *
     * @param \AppBundle\Entity\Song $song
     *
     * @return SongListItem
     */
    public function setSong(\AppBundle\Entity\Song $song = null)
    {
        $this->song = $song;

        return $this;
    }

    /**
     * Get song
     *
     * @return \AppBundle\Entity\Song
     */
    public function getSong()
    {
        return $this->song;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->songParams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transpose = 0;
    }

    /**
     * Add songParam
     *
     * @param \AppBundle\Entity\SongParams $songParam
     *
     * @return SongListItem
     */
    public function addSongParam(\AppBundle\Entity\SongParams $songParam)
    {
        $this->songParams[] = $songParam;

        return $this;
    }

    /**
     * Remove songParam
     *
     * @param \AppBundle\Entity\SongParams $songParam
     */
    public function removeSongParam(\AppBundle\Entity\SongParams $songParam)
    {
        $this->songParams->removeElement($songParam);
    }

    /**
     * Get songParams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSongParams()
    {
        return $this->songParams;
    }

    /**
     * Set transpose.
     *
     * @param int $transpose
     *
     * @return SongListItem
     */
    public function setTranspose($transpose)
    {
        $this->transpose = $transpose;

        return $this;
    }

    /**
     * Get transpose.
     *
     * @return int
     */
    public function getTranspose()
    {
        return $this->transpose;
    }
}

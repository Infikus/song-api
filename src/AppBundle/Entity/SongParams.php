<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SongParams
 *
 * @ORM\Table(name="song_params")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SongParamsRepository")
 */
class SongParams
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"songParams"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="capo", type="integer")
     * @Assert\NotNull()
     * @Assert\GreaterThanOrEqual(value=0)
     * @Assert\LessThanOrEqual(value=12)
     * @Serializer\Groups({"songParams"})
     */
    private $capo;

    /**
     * Unused if the SongParams is linked to a SongListItem, because each SongListItem has its own transpose attribute
     *
     * @var int
     *
     * @ORM\Column(name="transpose", type="integer")
     * @Assert\NotNull()
     * @Assert\GreaterThanOrEqual(value=-6)
     * @Assert\LessThanOrEqual(value=5)
     * @Serializer\Groups({"songParams"})
     */
    private $transpose;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Groups({"songParams_user"})
     */
    private $user;

    /**
     * @var Song
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Song", inversedBy="songParams")
     * @Serializer\Groups({"songParams_song"})
     */
    private $song;

    /**
     * @var SongListItem
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SongListItem", inversedBy="songParams")
     * @Serializer\Groups({"songParams_songListItem"})
     */
    private $songListItem;

    public function __construct()
    {
        $this->capo = 0;
        $this->transpose = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set capo
     *
     * @param integer $capo
     *
     * @return SongParams
     */
    public function setCapo($capo)
    {
        $this->capo = max(0, min(12, $capo));

        return $this;
    }

    /**
     * Get capo
     *
     * @return int
     */
    public function getCapo()
    {
        return $this->capo;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return SongParams
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set song
     *
     * @param \AppBundle\Entity\Song $song
     *
     * @return SongParams
     */
    public function setSong(\AppBundle\Entity\Song $song = null)
    {
        $this->song = $song;

        return $this;
    }

    /**
     * Get song
     *
     * @return \AppBundle\Entity\Song
     */
    public function getSong()
    {
        return $this->song;
    }

    /**
     * Set songListItem
     *
     * @param \AppBundle\Entity\SongListItem $songListItem
     *
     * @return SongParams
     */
    public function setSongListItem(\AppBundle\Entity\SongListItem $songListItem = null)
    {
        $this->songListItem = $songListItem;

        return $this;
    }

    /**
     * Get songListItem
     *
     * @return \AppBundle\Entity\SongListItem
     */
    public function getSongListItem()
    {
        return $this->songListItem;
    }

    /**
     * Set transpose.
     *
     * @param int $transpose
     *
     * @return SongParams
     */
    public function setTranspose($transpose)
    {
        $this->transpose = $transpose;

        return $this;
    }

    /**
     * Get transpose.
     *
     * @return int
     */
    public function getTranspose()
    {
        return $this->transpose;
    }
}

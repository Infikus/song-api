<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"team"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Serializer\Groups({"team"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Serializer\Groups({"team"})
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Serializer\Groups({"team"})
     */
    private $createdAt;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="TeamMember", mappedBy="team", cascade={"persist", "remove"})
     * @Serializer\Groups({"team_teamMembers"})
     */
    private $teamMembers;

    /**
     * @var bool
     *
     * @ORM\Column(name="private", type="boolean")
     * @Serializer\Groups({"team"})
     */
    private $private;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Song", mappedBy="team", cascade={"remove"})
     * @Serializer\Groups({"team_songs"})
     */
    private $songs;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="SongList", mappedBy="team", cascade={"remove"})
     * @Serializer\Groups({"team_songLists"})
     */
    private $songLists;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Team
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teamMembers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime("now");
        $this->private = false;
        $this->description = "";
    }

    /**
     * Set private
     *
     * @param boolean $private
     *
     * @return Team
     */
    public function setPrivate($private)
    {
        $this->private = $private;

        return $this;
    }

    /**
     * Get private
     *
     * @return boolean
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Add song
     *
     * @param \AppBundle\Entity\Song $song
     *
     * @return Team
     */
    public function addSong(\AppBundle\Entity\Song $song)
    {
        $this->songs[] = $song;

        return $this;
    }

    /**
     * Remove song
     *
     * @param \AppBundle\Entity\Song $song
     */
    public function removeSong(\AppBundle\Entity\Song $song)
    {
        $this->songs->removeElement($song);
    }

    /**
     * Get songs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSongs()
    {
        return $this->songs;
    }

    /**
     * Add songList
     *
     * @param \AppBundle\Entity\SongList $songList
     *
     * @return Team
     */
    public function addSongList(\AppBundle\Entity\SongList $songList)
    {
        $this->songLists[] = $songList;

        return $this;
    }

    /**
     * Remove songList
     *
     * @param \AppBundle\Entity\SongList $songList
     */
    public function removeSongList(\AppBundle\Entity\SongList $songList)
    {
        $this->songLists->removeElement($songList);
    }

    /**
     * Get songLists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSongLists()
    {
        return $this->songLists;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Team
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add teamMember
     *
     * @param \AppBundle\Entity\TeamMember $teamMember
     *
     * @return Team
     */
    public function addTeamMember(\AppBundle\Entity\TeamMember $teamMember)
    {
        $this->teamMembers[] = $teamMember;

        return $this;
    }

    /**
     * Remove teamMember
     *
     * @param \AppBundle\Entity\TeamMember $teamMember
     */
    public function removeTeamMember(\AppBundle\Entity\TeamMember $teamMember)
    {
        $this->teamMembers->removeElement($teamMember);
    }

    /**
     * Get teamMembers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeamMembers()
    {
        return $this->teamMembers;
    }
}

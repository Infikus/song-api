<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TeamMember
 *
 * @ORM\Table(name="team_member")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamMemberRepository")
 */
class TeamMember
{
    const TEAM_MASTER = 0;
    const TEAM_MEMBER = 1; // Can't remove a team_member or delete the team
    const WAITING_ANSWER = 10; // Can't do anything. Just waiting that a master accept or decline the invitation

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"teamMember"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="role", type="integer")
     * @Serializer\Groups({"teamMember"})
     */
    private $role;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="teamMembers", cascade={"persist"})
     * @Assert\NotNull()
     * @Serializer\Groups({"teamMember_user"})
     */
    private $user;

    /**
     * @var Team
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="teamMembers", cascade={"persist"})
     * @Assert\NotNull()
     * @Serializer\Groups({"teamMember_team"})
     */
    private $team;

    public function __construct()
    {
        $this->role = self::TEAM_MEMBER;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param integer $role
     *
     * @return TeamMember
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return int
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return TeamMember
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     *
     * @return TeamMember
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }
}

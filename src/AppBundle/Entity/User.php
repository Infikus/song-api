<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"user"})
     */
    protected $id;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TeamMember", mappedBy="user", cascade={"persist", "remove"})
     * @Serializer\Groups({"user_teamMembers"})
     */
    private $teamMembers;

    /**
     * @var TeamMember
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TeamMember", cascade={"persist"})
     * @Serializer\Groups({"user_lastTeamMember"})
     */
    private $lastTeamMember;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add teamMember
     *
     * @param \AppBundle\Entity\TeamMember $teamMember
     *
     * @return User
     */
    public function addTeamMember(\AppBundle\Entity\TeamMember $teamMember)
    {
        $this->teamMembers[] = $teamMember;

        return $this;
    }

    /**
     * Remove teamMember
     *
     * @param \AppBundle\Entity\TeamMember $teamMember
     */
    public function removeTeamMember(\AppBundle\Entity\TeamMember $teamMember)
    {
        $this->teamMembers->removeElement($teamMember);
    }

    /**
     * Get teamMembers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeamMembers()
    {
        return $this->teamMembers;
    }

    /**
     * Set lastTeamMember
     *
     * @param \AppBundle\Entity\TeamMember $lastTeamMember
     *
     * @return User
     */
    public function setLastTeamMember(\AppBundle\Entity\TeamMember $lastTeamMember = null)
    {
        $this->lastTeamMember = $lastTeamMember;

        return $this;
    }

    /**
     * Get lastTeamMember
     *
     * @return \AppBundle\Entity\TeamMember
     */
    public function getLastTeamMember()
    {
        return $this->lastTeamMember;
    }
}

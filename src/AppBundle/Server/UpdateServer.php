<?php
namespace AppBundle\Server;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UpdateServer implements MessageComponentInterface
{

    protected $connections = array();

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function findIndexByConn(ConnectionInterface $conn) {
        foreach($this->connections as $key => $conn_element){
            if($conn === $conn_element["conn"]){
                return $key;
            }
        }
        return -1;
    }

    protected function findAllIndexByUserId(int $userId) {
        $res = [];
        foreach($this->connections as $key => $conn_element){
            if($userId === $conn_element["userId"]){
                echo "found\n";
                $res[] = $key;
            }
        }
        return $res;
    }

    protected function getUsersByTeam($teamId) {
        $em = $this->container->get('doctrine')->getManager();

        $userRepo = $em->getRepository('AppBundle:User');

        return $userRepo->getByTeam($teamId);
    }

    /**
     * A new websocket connection
     *
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections[] = [ "conn" => $conn, "userId" => null ];
        echo "New connection \n";
    }

    /**
     * Handle message sending
     *
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        echo "message received :\n";
        $data = json_decode($msg, true);
        print_r($data);
        if (!$data) {
            return ;
        }
        $index = $this->findIndexByConn($from);
        if ($index < 0) {
            echo "Error: 'from' not found\n";
            return ;
        }
        switch($data['type']) {
            case 'SUBSCRIBE':
                if (array_key_exists("payload", $data) &&
                    array_key_exists("userId", $data["payload"]) &&
                    is_numeric($data["payload"]["userId"])) {
                    $userId = $data["payload"]["userId"];
                    $this->connections[$index]["userId"] = $userId;
                    echo "connection updated (new userId=".$userId.")\n";
                }
                break;
            case 'DATA_UPDATED':
                /*
                 * format request:
                 * {
                 *   "type": "DATA_UPDATED",
                 *   "teamId": [int],
                 *   "userId": [int],
                 *   "payload": {
                 *     "type": [string],
                 *     "payload": {
                 *       ...
                 *     }
                 *   }
                 * }
                 *
                 * format response:
                 * {
                 *   "type": [string],
                 *   "payload": {
                 *     ...
                 *   }
                 * }
                 */
                if (array_key_exists("payload", $data) &&
                    array_key_exists("teamId", $data) &&
                    is_numeric($data["teamId"])) {
                    $response = $data["payload"];
                    $sendBy = array_key_exists("userId", $data) && is_numeric($data["userId"]) ? $data["userId"] : null;
                    $users = $this->getUsersByTeam($data["teamId"]);
                    foreach($users as $user) {
                        $allId = $this->findAllIndexByUserId($user->getId());
                        echo "user :".$user->getId()."\n";
                        //if (&& $user->getId() !== $sendBy) {
                        foreach($allId as $id) {
                            $this->connections[$id]["conn"]->send(json_encode($response));
                        }
                    }
                }
                break;
            default:
                echo $data['type']."is unknown";
        }
    }

    /**
     * A connection is closed
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        echo "connection closed.";
        foreach($this->connections as $key => $conn_element){
            if($conn === $conn_element["conn"]){
                unset($this->connections[$key]);
                break;
            }
        }
        echo "Left: ".count($this->connections)."\n";
    }

    /**
     * Error handling
     *
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->send("Error : " . $e->getMessage());
        $conn->close();
    }
}
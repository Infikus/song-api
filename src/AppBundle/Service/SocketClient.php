<?php

namespace AppBundle\Service;

class SocketClient {
    const url = "ws://localhost:5000";

    public function sendDataUpdated($teamId, $userId, $payload) {
        \Ratchet\Client\connect(self::url)
            ->then(function($conn) use ($teamId, $userId, $payload) {
                $data = [
                    "type" => "DATA_UPDATED",
                    "teamId" => $teamId,
                    "userId" => $userId,
                    "payload" => $payload,
                ];
                $conn->send(json_encode($data));
                $conn->close();
            }, function($e) {
                echo "Could not connect: ($e->getMessage())\n";
            });
    }
}